import com.codeborne.selenide.Configuration
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.WebDriverRunner
import org.openqa.selenium.By
import com.codeborne.selenide.Selenide.open

fun initialize() {
    Configuration.startMaximized = false
    Configuration.holdBrowserOpen = true
    Configuration.browser = WebDriverRunner.CHROME
}

fun login() {
    open("https://www.instagram.com/accounts/login/")

    //レコンパンス
    Selenide.getElement(By.name("username")).value = "doux_recompense"
    Selenide.getElement(By.name("password")).value = "droeuc1230"

    //舞洲店
    //Selenide.getElement(By.name("username")).value = "familymart_maishima"
    //Selenide.getElement(By.name("password")).value = "maishima7"

    Selenide.getElement(By.className("HmktE")).submit()

    Selenide.sleep(3000)
}

fun fav() {
    for (k in 0..30) {
        //レコンパンス
        var searchString = arrayOf("大阪ランチ", "大阪スイーツ", "カフェ", "中之島", "肥後橋", "大阪カフェ", "渡辺橋", "フェスティバルホール")

        //舞洲店
        //var searchString = arrayOf("舞洲", "舞洲スポーツアイランド", "舞洲ゆり園", "コンビニスイーツ", "コンビニ", "ファミマ")

        var waittime = 1000

        for (i in 0 until searchString.size) {
            try {
                println("ハッシュタグ #" + searchString[i])

                // FOR DEBUG
                //if (i == 1) {
                //throw Exception("例外テスト")
                //}

                Selenide.open("https://www.instagram.com/explore/tags/" + searchString[i] + "/")

                Selenide.getElements(By.className("eLAPa"))[9].click()

                Selenide.sleep(waittime.toLong())

                for (j in 1..10) {

                    Selenide.sleep(waittime.toLong())

                    Selenide.getElement(By.className("coreSpriteHeartOpen")).click()  //fav

                    Selenide.sleep(waittime.toLong())

                    Selenide.getElement(By.className("HBoOv")).click()  //next

                    Selenide.sleep(50000)
                }
            }
            catch (e: NoSuchElementException) {
                println("エラーをキャッチしました。")
                println(e.message)
            }
        }
    }
}

fun remove() {
    Selenide.open("https://www.instagram.com/doux_recompense/")

    Selenide.getElements(By.className("g47SY"))[2].click()
    var followUser = Selenide.getElements(By.className("FPmhX")).texts()

    println(followUser)

//    for (k in 0..20) {
//        print(k )
//        Selenide.getElements(By.className("v1Nh3"))[k].scrollTo()
//    }
}

fun main(args: Array<String>) {
    println("start")

    initialize()
    login()
    fav()
    //remove()

    println("end")

}